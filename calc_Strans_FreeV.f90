! written by jxzou at 20190807: calculate translational entropy using free volume model
! If you find any bugs or have any suggestion, please email to me via njumath@sina.cn

!------------------------------------------------------------------------------------------
!                              Citation:
! Whitesides et al. J. Org. Chem. 1998, 63, 3821, DOI: 10.1021/jo970944f
!
! If you use volume calculated by Multiwfn, you should also cite
!  T. Lu and F. Chen, J. Mol. Graph. Model. 2012, 38, 314, DOI: 10.1016/j.jmgm.2012.07.004
!------------------------------------------------------------------------------------------

!------------------------------------------------------
! Compile:
!  gfortran calc_Strans_FreeV.f90 -o calc_Strans_FreeV
!------------------------------------------------------

! Input parameters:
!  M_solute: Molecular mass of solute   (g/mol)
!  M_solvent: Molecular mass of solvent (g/mol)
!  V_solvent: Volume of solute          (Bohr^3)
!  rou_solvent: density of solvent      (g/cm3)
!  T: Temperature (K)
!  P: Pressure    (atm)
!  G: Gibbs free energy using ideal gas model (a.u.)

! NIST physical constants, see https://physics.nist.gov/cgi-bin/cuu/CCsearch?ShowFirst=Browse
module phys_const
 implicit none
 real(kind=8), parameter :: kB    = 1.380649d0       ! 1d-23 J/K
 real(kind=8), parameter :: h     = 6.62607015d0     ! 1d-34 Js
 real(kind=8), parameter :: NA    = 6.02214076d0     ! 1d23 mol-1
 real(kind=8), parameter :: R     = 8.314462618d0    ! J/(mol*K)
 real(kind=8), parameter :: Bohr  = 0.529177210903d0 ! 1d-10 m
 real(kind=8), parameter :: atm   = 101325d0         ! Pa
 real(kind=8), parameter :: PI    = 3.14159265359d0  ! math constant pi
 real(kind=8), parameter :: J2cal = 0.239005736d0
 real(kind=8), parameter :: J2au  = 3.80879803d-7
! the exponent part is considered during calculating
end module phys_const

program main
 use phys_const, only: J2cal, J2au
 implicit none
 integer :: i, j, k, fid
 real(kind=8) :: M_solute, M_solvent, V_solvent, rou_solvent
 real(kind=8) :: T, P, G, S_trans1, S_trans2, e1, e2
 character(len=24) :: str(7)
 character(len=240) :: buf, txtname

 i = iargc()
 if(i /= 1) then
  write(*,'(A)') 'Wrong command line parameters!'
  write(*,'(A)') 'Format:  ./calc_Strans_FreeV xx.txt'
  write(*,'(A)') 'Example: ./calc_Strans_FreeV He_in_water.txt'
  stop
 end if

 txtname = ' '
 call getarg(1, txtname)
 open(newunit=fid,file=TRIM(txtname),status='old',position='rewind')

 str = ' '
 buf = ' '
 do i = 1, 7
  read(fid,'(A)',iostat=j) buf
  if(j /= 0) exit
  k = index(buf, '=')
  read(buf(k+1:),*) str(i)
 end do
 close(fid)

 if(j /= 0) then
  write(*,'(A)') 'ERROR in reading file '//TRIM(txtname)
  stop
 end if

 read(str(1),*) M_solute     ! g/mol
 read(str(2),*) M_solvent    ! g/mol
 read(str(3),*) V_solvent    ! Bohr^3
 read(str(4),*) rou_solvent  ! g/cm3
 read(str(5),*) T ! K
 read(str(6),*) P ! atm
 read(str(7),*) G ! a.u.

 write(*,'(/,A)') 'Calculated translational entropy:'

 call calc_Strans_ideal(M_solute, T, P, S_trans1)
 write(*,'(/,A,F17.9)') 'Ideal gas  [  J/(mol*K)]:', S_trans1
 write(*,'(A,F17.9)')   'Ideal gas  [cal/(mol*K)]:', S_trans1*J2cal
 e1 = -T*S_trans1*J2au
 write(*,'(A,F17.9)')   'Ideal gas          [a.u]:', e1

 call calc_Strans_FreeV(M_solute, M_solvent, V_solvent, rou_solvent, T, P, S_trans2)
 write(*,'(/,A,F17.9)') 'Free Volume[  J/(mol*K)]:', S_trans2
 write(*,'(A,F17.9)')   'Free Volume[cal/(mol*K)]:', S_trans2*J2cal
 e2 = -T*S_trans2*J2au
 write(*,'(A,F17.9)')   'Free Volume       [a.u.]:', e2

 write(*,'(/,A,F17.9)') 'Correction to tranlational entropy [a.u.]:', e2 - e1
 write(*,'(A,F15.6)') 'Old Gibbs free energy:', G
 write(*,'(A,F15.6)') 'New Gibbs free energy:', G + (e2-e1)
 stop
end program main

! calculate translational entropy using Sackur-Tetrode equation with ideal gas model
subroutine calc_Strans_ideal(M, T, P0, S_trans)
 use phys_const
 implicit none
 real(kind=8) :: const, qt, P
 real(kind=8), intent(in) :: M, T, P0
 real(kind=8), intent(out) :: S_trans

 P = P0*atm   ! atm to Pa

 const = ( 2d0*PI*kB**(5.0d0/3.0d0)/(NA*h**2) )*( 10d0**(11d0/3d0) )
 qt = ((const*M)**1.5d0)*(T**2.5d0)/P

 S_trans = R*(DLOG(qt) + 2.5d0)
 return
end subroutine calc_Strans_ideal

! calculate translational entropy using Sackur-Tetrode equation with free volume model
subroutine calc_Strans_freeV(M_solute,M_solvent,V_solvent,rou_solvent, T, P0, S_trans)
 use phys_const
 implicit none
 real(kind=8) :: c, a, d, Vfree, qt, const, P
 real(kind=8), intent(in) :: M_solute,M_solvent,V_solvent,rou_solvent, T, P0
 real(kind=8), intent(out) :: S_trans

 P = P0*atm   ! atm to Pa

 ! rou_solvent: g/cm3
 ! M_solvent: g/mol
 ! c: mol/cm3
 c = rou_solvent/M_solvent
 d = (10d0/(c*NA))**(1d0/3d0)   ! in Anstrom
 a = Bohr*V_solvent**(1d0/3d0)  ! in A
 Vfree = 8d0*(d-a)**3           ! in A^3

 write(*,'(/,A6,F8.3,1X,A3)') 'Vfree=', Vfree, 'A^3'

 const = 2d0*PI*kB/(NA*h**2)
 qt = Vfree*(const*M_solute*T)**1.5d0

 S_trans = R*(DLOG(qt) + 2.5d0)
 return
end subroutine calc_Strans_freeV

